<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\C_pages;
use App\Http\Controllers\C_users;
use App\Http\Controllers\C_users_member;
use App\Http\Controllers\C_auth;
use App\Http\Controllers\C_system_menu;
use App\Http\Controllers\C_ticket;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/template', function (Request $request) {
//     $response = array();
//     $response['style'] = "1";
//     $response['ressort'] = app(\App\Http\Controllers\C_pages::class)->getRessort();
//     $response['unit_pelayanan'] = app(\App\Http\Controllers\C_pages::class)->getPelayanan();
//     return view('public.template')->with($response);
// })->middleware(['getRessort', 'getPelayanan']);



$router->group(['prefix' => 'login'], function () use ($router) {
    Route::get('/', [C_pages::class, 'loginPage']);
});

$router->group(['prefix' => 'logout'], function () use ($router) {
    Route::get('/', [C_auth::class, 'logoutPage']);
});

$router->group(['prefix' => 'admin', 'as'=>'admin.', 'middleware' => ['authSession', 'getTrustee']], function () use ($router) {
    Route::get('/', [C_pages::class, 'cpanel']);

    $router->group(['prefix' => 'users'], function () use ($router) {
        Route::get('/', [C_pages::class, 'pageUsers']);
        Route::get('/form', [C_users::class, 'form']);
        Route::get('/form/{id}', [C_users::class, 'form']);
    
        $router->group(['prefix' => 'parent'], function () use ($router) {
            Route::get('/', [C_pages::class, 'pageUsersParent']);
        });

        $router->group(['prefix' => 'member'], function () use ($router) {
            Route::get('/', [C_pages::class, 'pageUsersMember']);
            Route::get('/form', [C_users_member::class, 'form']);
            Route::get('/form/{id}', [C_users_member::class, 'form']);
        });
    });
    
    $router->group(['prefix' => 'system_group'], function () use ($router) {
        Route::get('/', [C_pages::class, 'pageSystemGroup']);
    });
    
    $router->group(['prefix' => 'system_member'], function () use ($router) {
        Route::get('/', [C_pages::class, 'pageSystemMember']);
    });
    
    $router->group(['prefix' => 'system_role'], function () use ($router) {
        Route::get('/', [C_pages::class, 'pageSystemRole']);
    });
    
    $router->group(['prefix' => 'system_menu'], function () use ($router) {
        Route::get('/', [C_pages::class, 'pageSystemMenu']);
        Route::get('/form', [C_system_menu::class, 'form']);
        Route::get('/form/{id}', [C_system_menu::class, 'form']);
    });
    
    $router->group(['prefix' => 'ticket'], function () use ($router) {
        Route::get('/', [C_pages::class, 'pageTicket']);
        Route::get('/form', [C_ticket::class, 'form']);
        Route::get('/form/{id}', [C_ticket::class, 'form']);
    });

});

$router->group(['prefix' => '/'], function () use ($router) {
    Route::get('/', [C_pages::class, 'pageDataTicket']);
});


$router->group(['prefix' => 'ticket_detail'], function () use ($router) {
    Route::get('/{menu}', [C_pages::class, 'pageDataTicketDetail']);
});
