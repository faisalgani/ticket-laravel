<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\C_auth;
use App\Http\Controllers\C_users;
use App\Http\Controllers\C_users_member;
use App\Http\Controllers\C_system_group;
use App\Http\Controllers\C_system_member;
use App\Http\Controllers\C_system_menu;
use App\Http\Controllers\C_system_role;
use App\Http\Controllers\C_ticket;
use App\Http\Controllers\C_ticket_sell;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

$router->group(['prefix' => 'v1'], function () use ($router) {
    $router->group(['prefix' => 'auth'], function () use ($router) {
        Route::post('/login', [C_auth::class, 'login']);
        Route::post('/update', [C_auth::class, 'update']);
        Route::post('/delete', [C_auth::class, 'delete']);
    });

    $router->group(['prefix' => 'users'], function () use ($router) {
        Route::get('/read', [C_users::class, 'getData']);
        Route::get('/store', [C_users::class, 'getStore']);
        Route::post('/create', [C_users::class, 'create']);
        Route::post('/update', [C_users::class, 'update']);
        Route::post('/delete', [C_users::class, 'delete']);
        
        $router->group(['prefix' => 'member'], function () use ($router) {
            Route::get('/read', [C_users_member::class, 'getData']);
            Route::get('/store', [C_users_member::class, 'getStore']);
            Route::post('/create', [C_users_member::class, 'create']);
            Route::post('/update', [C_users_member::class, 'update']);
            Route::post('/delete', [C_users_member::class, 'delete']);
        });
    });

    $router->group(['prefix' => 'system_group'], function () use ($router) {
        Route::get('/read', [C_system_group::class, 'getData']);
        Route::get('/store', [C_system_group::class, 'getStore']);
        Route::get('/store/{id}', [C_system_group::class, 'getStore']);
        Route::post('/create', [C_system_group::class, 'create']);
        Route::post('/update', [C_system_group::class, 'update']);
        Route::post('/delete', [C_system_group::class, 'delete']);
    });

    $router->group(['prefix' => 'system_member'], function () use ($router) {
        Route::get('/read', [C_system_member::class, 'getData']);
        Route::get('/store', [C_system_member::class, 'getStore']);
        Route::get('/store/{id}', [C_system_member::class, 'getStore']);
        Route::post('/create', [C_system_member::class, 'create']);
        Route::post('/update', [C_system_member::class, 'update']);
        Route::post('/delete', [C_system_member::class, 'delete']);
        Route::post('/deleteByGroup', [C_system_member::class, 'deleteByGroup']);
    });

    $router->group(['prefix' => 'system_menu'], function () use ($router) {
        Route::get('/read', [C_system_menu::class, 'getData']);
        Route::get('/store', [C_system_menu::class, 'getStore']);
        Route::get('/store/{id}', [C_system_menu::class, 'getStore']);
        Route::post('/create', [C_system_menu::class, 'create']);
        Route::post('/update', [C_system_menu::class, 'update']);
        Route::post('/delete', [C_system_menu::class, 'delete']);
        Route::post('/deleteByGroup', [C_system_menu::class, 'deleteByGroup']);
        Route::post('/update/position', [C_system_menu::class, 'reorder']);
    });

    $router->group(['prefix' => 'system_role'], function () use ($router) {
        Route::get('/read', [C_system_role::class, 'getData']);
        Route::get('/store', [C_system_role::class, 'getStore']);
        Route::get('/store/{id}', [C_system_role::class, 'getStore']);
        Route::post('/create', [C_system_role::class, 'create']);
        Route::post('/update', [C_system_role::class, 'update']);
        Route::post('/delete', [C_system_role::class, 'delete']);
        Route::post('/deleteByGroup', [C_system_role::class, 'deleteByGroup']);
    });

    $router->group(['prefix' => 'ticket'], function () use ($router) {
        Route::get('/read', [C_ticket::class, 'getData']);
        Route::get('/store', [C_ticket::class, 'getStore']);
        Route::get('/store/{id}', [C_ticket::class, 'getStore']);
        Route::post('/create', [C_ticket::class, 'create']);
        Route::post('/update', [C_ticket::class, 'update']);
        Route::post('/delete', [C_ticket::class, 'delete']); 
        Route::post('/upload_image', [C_ticket::class, 'upload_image']);
        Route::post('/post', [C_ticket::class, 'posting']);
        Route::post('/buy', [C_ticket_sell::class, 'create']);
    });
});