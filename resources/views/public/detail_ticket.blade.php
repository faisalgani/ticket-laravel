@include('public.partials.pluginCSS') 
@include('public.partials.TopbarNavigation')
<script
  src="https://code.jquery.com/jquery-2.2.4.min.js"
  integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
  crossorigin="anonymous">
</script>
<?php $response = $data[0]; //
//var_dump($data); ?>
<?php foreach ($data as  $value) { 
    if($value->discount != "" ){
        $discount = (int) $value->price /  (int)$value->discount;
        $after_discount = $value->price - $discount;    
    }else{
        $after_discount = $value->price;
    }
    
?>
<section class="text-gray-700 body-font overflow-hidden bg-white">
  <div class="container px-5 py-24 mx-auto">
    <div class="lg:w-4/5 mx-auto flex flex-wrap">
      <img alt="ecommerce" class="lg:w-1/2 w-full object-cover object-center rounded border border-gray-200" src="<?php echo $value->cover_ticket ?>">
      <div class="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
        <h2 class="text-sm title-font text-gray-500 tracking-widest"></h2>
        <h1 class="text-gray-900 text-3xl title-font font-medium mb-1"><?php echo $value->event_name ?></h1>
        <div class="flex mb-4">
          <span class="flex items-center">
          <p class="mt-1 text-xs text-black-800">
            <?php echo  $value->event_date; ?>
            </p>
           
          </span>

        </div>
        <p class="leading-relaxed"><?php echo $value->event_desc ?> </p>
        <div class="flex mt-6 items-center pb-5 border-b-2 border-gray-200 mb-5">
          <div class="flex">
            <span class="mr-3">Jam </span>
            <?php echo date ('H:i',strtotime($value->event_time_start));?> - <?php echo date ('H:i',strtotime($value->event_time_end));?>
          </div>
          
        </div>
        <div class="flex mt-6 items-center pb-5 border-b-2 border-gray-200 mb-5">
          <!-- 
            
           -->
          <div class="flex ml-6 items-center">
            <span class="mr-3">Jumlah</span>
            <div class="relative">
            <input class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="jumlah" type="text" placeholder="jumlah">
              
              </span>
            </div>
          </div>
        </div>
        <div class="flex">
          <span class="title-font font-medium text-2xl text-gray-900">Rp. <?php echo number_format($after_discount) ?></span>
          <button class="flex ml-auto text-white bg-blue-500 border-0 py-2 px-6 focus:outline-none hover:bg-red-600 rounded" onclick="buy_ticket('<?php echo $value->id ?>')">Pesan</button>
        </div>
      </div>
    </div>
  </div>
</section>

<?php } ?>

<script>
  function buy_ticket(id){
       // window.location='/ticket/buy'+id;
       $.ajax({
          url : "<?php echo URL::to('/api/v1/ticket/buy'); ?>",
          type: "POST",
          data: { "id_ticket" : id,"sold" : $("#jumlah").val()},
          dataType: "JSON",
          success: function(data){
              console.log(data);
              if(data.code == 200){
                alert(data.message);
                window.location='/';
              }
          },
          error: function (jqXHR, textStatus, errorThrown){        

          }
      });   
    }
</script>
