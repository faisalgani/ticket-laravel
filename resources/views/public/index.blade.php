
@include('public.partials.pluginCSS') 
@include('public.partials.TopbarNavigation') 
@include('public.partials.MainSliders')     
<?php $response = $data[0]; //
//var_dump($data); ?>
   
<section>
  <div class="px-4 py-16 mx-auto max-w-screen-xl sm:px-6 lg:px-8">
    <ul class="space-y-4">
    <?php foreach ($data as  $value) { 
       if($value->discount != "" ){
        $discount = (int) $value->price /  (int)$value->discount;
       }else{
         $discount = 0;
       }
      
      ?>
      <li class="p-4 border-0 border-black sm:items-center sm:justify-between sm:flex">
        <div class="flex items-center sm:flex-1">
          <a href="" class="flex-shrink-0 block">
            <img class="w-50 h-50" src="<?php echo $value->cover_ticket ?>" alt="" />
          </a>

          <div class="max-w-xl ml-6">
            <a href="" class="text-lg font-medium">
              <?php echo $value->event_name; ?>
            </a>

            <p class="mt-1 text-xs text-gray-500">
            <?php echo  $value->event_date; ?>
            </p>

            <p class="mt-1 text-xs text-gray-500">
            <?php echo date ('H:i',strtotime($value->event_time_start));?>  -  <?php echo date ('H:i',strtotime($value->event_time_end));?>
            </p>
          <?php if($value->discount == "" || $value->discount == null){ ?>
              <p class="mt-4 font-medium text-gray-700">Rp. <?php echo number_format($value->price - $discount) ; ?></p>
      <?php     }else{ ?>
                <p class="mt-4 font-medium text-gray-700">Rp. <?php echo number_format($value->price - $discount) ; ?> <s class="opacity-50">Rp. <?php echo number_format($value->price); ?></s></p>
        <?php } ?>
            

           
          </div>
        </div>

        <div class="mt-4 lg:items-center lg:mt-0 lg:flex">
          <div class="flex justify-between p-2 border-0 border-black space-x-2">
            <button
              type="button"
              id="btn-detail-ticket"
              onclick="ticket_detail('<?php echo $value->id ?>')"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            ><i class="fas fa-info"></i>
             
            </button>
            <!-- <a href="/ticket_detail/<?php echo $value->id ?> " class="btn btn-primary">
                <i class="fas fa-info"></i> Detail
            </a> -->
            
          </div>
          <?php  
           } ?>
         
        </div>
      </li>
    </ul>
  </div>
</section>

<script>
  function ticket_detail(id){
    console.log(id);
        window.location='/ticket_detail/'+id;
    }
</script>
