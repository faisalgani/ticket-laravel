<!-- Javascript -->
<script type="text/javascript" src="{{asset('assets/front_end/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/animsition.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/plugins.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/countTo.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/flexslider.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/owlCarousel.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/cube.portfolio.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/main.js')}}"></script>

<!-- Revolution Slider -->
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/jquery.themepunch.tools.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/jquery.themepunch.revolution.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/js/rev-slider.js')}}"></script>
<!-- Load Extensions only on Local File Systems ! The following part can be removed on Server for On Demand Loading -->  
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.carousel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.kenburn.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.migration.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.parallax.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script type="text/javascript" src="{{asset('assets/front_end/includes/rev-slider/js/extensions/revolution.extension.video.min.js')}}"></script>
@stack('pluginJS')
<script src="{{asset('js/app.js')}}"></script>