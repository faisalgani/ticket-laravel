import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import {encryptParameter} from '../../../service/ServiceParams';
import * as ExternalAPI from '../../../service/ExternalAPI';

var dataTreeJSON = [];
export default class DataTree extends Component {
    constructor(props) {
        super(props);
        this.state  = {
            disableBtnSave: false,
            showLoadingSave: false,
            disableBtnAdd: false,
            showLoading: false,
            disableBtn: false,
            url: props.url,
            id_user: props.id_user,
            pages: 0,
            total: 0,
            dataTree: [],
            ktdataTree:{},
            limitChar: 100, 
            loading: false,
            datatable: {},
            header          : {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
                "Access-Control-Allow-Origin": "*",
                'X-CSRF-TOKEN': props.token,
            },
        }
        this.btnSave = this.btnSave.bind(this);
        this.btnChange = this.btnChange.bind(this);
    }

    async btnChange(e){
        location.href = this.state.url+"/admin/unit-pelayanan/form/"+$("#id").val();
    }

    async processReorder(index, element){
        var $this = this;
        return new Promise(async function(resolve) {
            var parameter = await encryptParameter({
                id: element.id,
                order: element.order,
                parent: element.parent,
            });
            var result = await ExternalAPI.post($this.state.url+'/api/v1/unit_pelayanan/update/position', $this.state.header, parameter);
            if(typeof(result.code) !== "undefined"){
                if(result.code == 200){
                    setTimeout(() => {
                        resolve();
                    }, 500);
                }
            }
        });
    }

    async btnSave(){
        await this.setState({
            disableBtnSave: true,
            showLoadingSave: true,
        })
        var jsonNodes = this.state.ktdataTree.jstree(true).get_json('#', { flat: false });
        await jsonNodes.forEach(async (element,index) => {
            index+=1;
            await this.ChildMenu(""+index, element, "");
        });

        for (let index = 0; index < dataTreeJSON.length; index++) {
            await this.processReorder(index, dataTreeJSON[index]);
        }
        await this.setState({
            disableBtnSave: false,
            showLoadingSave: false,
        })
        toastr.info("Posisi menu berhasil di update");
    }

    async ChildMenu(index, data, parent){
        if(data.children.length > 0){
            dataTreeJSON.push({
                id: data.id,
                order: index,
                menu: data.text,
                parent: parent,
            });

            await data.children.forEach(async (element, id) => {
                id+=1;
                await this.ChildMenu(index+"."+id, element, data.id);
            });
        }else{
            dataTreeJSON.push({
                id: data.id,
                order: index,
                menu: data.text,
                parent: parent,
            });
        }
    }

    async treeChild(id, data){
        var dataTree = [];
        await data.forEach(async (element, index) => {
            if(element.parent == id && (element.parent !== "" || element.parent !== null)){
                let obj = data.find(o => o.parent === element.id);
                var icon = "fa fa-file";
                if(typeof(obj) !== "undefined"){
                    icon = "fa fa-folder text-success";
                }

                dataTree.push({
                    id: element.id,
                    text: element.name,
                    parentID: element.parent,
                    children: await this.treeChild(element.id, data),
                });
            }
        });
        return dataTree;
    }

    async componentDidMount() {
        var $this = this;
        var parameter = await encryptParameter({
            id: "",
        });
        var result = await ExternalAPI.get(this.state.url+'/api/v1/departement/unit_pelayanan/read', this.state.header);
        var dataTree = [];
        if(typeof(result.code) !== "undefined"){
            if(result.code == 200 && result.count > 0){
                await result.data.forEach(async (element, index) => {
                    if(element.parent == null || element.parent == ""){    
                        let obj = result.data.find(o => o.parent === element.id);
                        var icon = "fa fa-file";
                        if(typeof(obj) !== "undefined"){
                            icon = "fa fa-folder text-success";
                        }

                        dataTree.push({
                            id: element.id,
                            text: element.name,
                            parentID: element.parent,
                            children: await this.treeChild(element.id, result.data),
                        });
                    }
                });
            }
        }

        await this.setState({
            dataTree: dataTree,
        })

        await this.setState({
            ktdataTree: await $("#kt_tree").jstree({
                "core": {
                    "themes": {
                        "responsive": false
                    },
                    // so that create works
                    "check_callback": true,
                    "data": $this.state.dataTree
                },
                "types": {
                    "default": {
                        "icon": "fa fa-file"
                    },
                    "file": {
                        "icon": "fa fa-file  text-success"
                    }
                },
                "state": {
                    "key": "demo2"
                },
                "plugins": ["dnd", "state", "types"]
            })
        });
        
        $('#kt_tree').on('select_node.jstree', async function(e, data) {
            await $("#id").val(data.node.original.id);
            await $("#idTxt").val(data.node.original.id);
            await $("#name").val(data.node.original.text);
        });
    }

    render() {
        return (
            <>
                <section>
  <div class="px-4 py-16 mx-auto max-w-screen-xl sm:px-6 lg:px-8">
    <ul class="space-y-4">
      <li class="p-4 border-0 border-black sm:items-center sm:justify-between sm:flex">
        <div class="flex items-center sm:flex-1">
          <a href="" class="flex-shrink-0 block">
            <img class="w-50 h-50" src="<?php echo $value->cover_ticket ?>" alt="" />
          </a>

          <div class="max-w-xl ml-6">
            <a href="" class="text-lg font-medium">
              
            </a>

            <p class="mt-1 text-xs text-gray-500">
           
            </p>

            <p class="mt-1 text-xs text-gray-500">
            
            </p>        
           
          </div>
        </div>

        <div class="mt-4 lg:items-center lg:mt-0 lg:flex">
          <div class="flex justify-between p-2 border-0 border-black space-x-2">
            <button
              type="button"
              id="btn-detail-ticket"
              class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded-full"
            ><i class="fas fa-info"></i>
             
            </button>
           
            
          </div>
          
         
        </div>
      </li>
    </ul>
  </div>
</section>
            </>
        );
    }
}

if (document.getElementById('cpanel-ticket-datalist')) {
    const app = document.getElementById('cpanel-ticket-datalist');
    ReactDOM.render(<DataTree {...app.dataset}/>, document.getElementById('cpanel-ticket-datalist'));
}
