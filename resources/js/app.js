/**
 * First we will load all of this project's JavaScript dependencies which
 * includes React and other helpers. It's a great starting point while
 * building robust, powerful web applications using React + Laravel.
 */

// require('./bootstrap');

/**
 * Next, we will create a fresh React component instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// require('./components/Example');
// ==================== LOGIN FORM
require('./pages/cpanel/LoginForm');

// ==================== USERS
require('./pages/cpanel/users/DataGrid');
require('./pages/cpanel/users/Form');

// ==================== SYSTEM GROUP
require('./pages/cpanel/system_group/DataGrid');
require('./pages/cpanel/system_group/FormModal');

// ==================== SYSTEM MEMBER
require('./pages/cpanel/system_member/Form');

// ==================== SYSTEM MENU
require('./pages/cpanel/system_menu/DataTree');
require('./pages/cpanel/system_menu/Form');

// ==================== SYSTEM GROUP
require('./pages/cpanel/system_role/DataGrid');
require('./pages/cpanel/system_role/Form');

// ==================== TICKET
require('./pages/cpanel/ticket/DataGrid');
require('./pages/cpanel/ticket/Form');
