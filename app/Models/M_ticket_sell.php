<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_ticket_sell extends Model
{
    use HasFactory;
	protected $keyType = 'string';
	public $incrementing = false;
	protected $table    = "ticket_sell";
	protected $fillable = [
        'id',
        'id_ticket',
        'sold',
        'created_by',
        'updated_by',
	];

	function ticket_sell_to_ticket(){
		return $this->hasOne('App\Models\M_ticket','id', 'id_ticket');
	}
}
