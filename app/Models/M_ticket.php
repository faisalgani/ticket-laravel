<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_ticket extends Model
{
    use HasFactory;
    protected $keyType = 'string';
	public $incrementing = false;
	protected $table    = "tickets";
	protected $fillable = [
        'id',
        'event_name',
        'cover_ticket',
        'event_desc',
        'event_location',
        'event_date',
        'event_time_start',
        'event_time_end',
        'price',
        'stock',
        'discount',
        'created_at',
        'updated_at',
        'state',
	];

	function ticket_to_ticket_sell(){
		return $this->hasOne('App\Models\M_ticket_sell','id', 'id_ticket');
	}
}
