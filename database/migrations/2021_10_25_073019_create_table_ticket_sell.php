<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTicketSell extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_sell', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id');
            $table->string('id_ticket');
            $table->foreign('id_ticket')->references('id')->on('tickets')
            ->onUpdate('cascade')
            ->onDelete('cascade');
            $table->integer('sold')->nullable();
            $table->date('created_at')->default(date("Y-m-d"));
            $table->date('updated_at')->default(date("Y-m-d"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_sell');
    }
}
