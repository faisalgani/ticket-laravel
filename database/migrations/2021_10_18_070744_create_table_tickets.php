<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableTickets extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->string('id');
            $table->primary('id');
            $table->string('event_name')->nullable();
            $table->string('cover_ticket')->nullable();
            $table->text('event_desc')->nullable();
            $table->string('event_location')->nullable();
            $table->date('event_date')->default(date("Y-m-d"));
            $table->time('event_time_start')->default(date("H:i:s"));
            $table->time('event_time_end')->default(date("H:i:s"));
            $table->string('price', 10)->nullable();
            $table->string('stock', 10)->nullable();
            $table->string('discount', 10)->nullable();
            $table->string('state')->default('draft');
            $table->date('created_at')->default(date("Y-m-d"));
            $table->date('updated_at')->default(date("Y-m-d"));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_tickets');
    }
}
